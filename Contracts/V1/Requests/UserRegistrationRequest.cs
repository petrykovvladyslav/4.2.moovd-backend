using System.ComponentModel.DataAnnotations;

namespace _4._2.moovd_backend.Controllers
{
    public class UserRegistrationRequest
    {
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
    }
}