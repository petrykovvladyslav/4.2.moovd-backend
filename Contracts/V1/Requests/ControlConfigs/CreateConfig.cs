using System;
using System.Collections.Generic;

namespace __4.__2.moovd_backend.Contracts.V1.Requests.ControlConfigs
{
    public class CreateConfig
    {
        public DateTime CreationDate { get; set; }
        public float Velocity { get; set; }
        public string GoalColor { get; set; }
        public int ColorInterval { get; set; }
        public bool IsBall { get; set; }
        public bool IsHorizontal { get; set; }
    }
}