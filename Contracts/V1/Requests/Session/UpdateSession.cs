using System;

namespace __4.__2.moovd_backend.Contracts.V1.Requests.Session
{
    public class UpdateSession
    {

        public DateTime SessionDate { get; set; }
        public int ActiveSession { get; set; }

        public string SessionUsers { get; set; }
        public string SessionResultId { get; set; }
    }
}