using System;

namespace __4.__2.moovd_backend.Contracts.V1.Requests
{
    public class CreateSessionUser
    {
        public string UserType { get; set; }
        public Guid UserId { get; set; }
        public DateTime LoginDateTime { get; set; }
        public DateTime? LogoutDateTime { get; set; }
    }
}