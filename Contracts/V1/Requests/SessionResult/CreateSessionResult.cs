using System;

namespace __4.__2.moovd_backend.Contracts.V1.Requests.SessionResult
{
    public class CreateSessionResult
    {
        public DateTime TimeStamp { get; set; }
        public int TaskNumber { get; set; }
        public int ClickNumber { get; set; }
        public int ResponseTime { get; set; }
        public float Speed { get; set; }
    }
}