namespace _4._2.moovd_backend.Controllers
{
    public class UserLoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}