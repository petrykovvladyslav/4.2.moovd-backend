namespace __4.__2.moovd_backend.Contracts.V1.Requests.Device
{
    public class CreateDevice
    {
        public string DeviceName { get; set; }
    }
}