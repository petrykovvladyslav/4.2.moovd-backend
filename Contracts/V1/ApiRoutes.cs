namespace _4._2.moovd_backend.Contracts
{
    public static class ApiRoutes
    {
        public const string Version = "v1";
        public const string Root = "api";
        public const string Base = Root + "/" + Version;

        public static class SessionUserRoute
        {
            public const string GetAll = Base + "/sessionUsers";
            public const string Create = Base + "/sessionUsers";
            public const string Get = Base + "/sessionUser/{sessionUserId}";
            public const string Update = Base + "/sessionUser/{sessionUserId}";
            public const string Delete = Base + "/sessionUser/{sessionUserId}";

        }
        public static class SessionRoute
        {
            public const string GetAll = Base + "/sessions";
            public const string Create = Base + "/sessions";
            public const string Get = Base + "/sessions/{sessionId}";
            public const string Update = Base + "/sessions/{sessionId}";
            public const string Delete = Base + "/sessions/{sessionId}";

        }

        public static class ConfigRoute
        {
            public const string GetAll = Base + "/configs";
            public const string Create = Base + "/configs";
            public const string Get = Base + "/configs/{configId}";
        }

        public static class DeviceRoute
        {
            public const string GetAll = Base + "/devices";
            public const string Create = Base + "/devices";
            public const string Get = Base + "/devices/{deviceId}";

        }
        public static class SessionResultRoute
        {
            public const string GetAll = Base + "/sessionResults";
            public const string Create = Base + "/sessionResults";
            public const string Get = Base + "/sessionResults/{sessionResultId}";

        }

        public static class Identity
        {
            public const string Login = Base + "/identity/login";
            public const string Register = Base + "/identity/register";
        }
    }
}