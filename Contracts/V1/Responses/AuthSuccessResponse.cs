namespace _4._2.moovd_backend.V1.Responses
{
    public class AuthSuccessResponse
    {
        public string Token { get; set; }
    }
}