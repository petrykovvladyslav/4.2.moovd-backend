using System.Collections.Generic;

namespace _4._2.moovd_backend.V1.Responses
{
    public class AuthFailedResponse
    {
        public IEnumerable<string> Errors { get; set; }
    }
}