using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace _4._2.moovd_backend.Installers{
    public interface IInstaller{
        void InstallServices(IServiceCollection services, IConfiguration configuration);
        void InstallServices(IServiceCollection services, object configuration);
    }
}