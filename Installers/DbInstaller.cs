using _4._2.moovd_backend.Data;
using _4._2.moovd_backend.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using __4.__2.moovd_backend.Services.SessionUserServices;
using _4._2.moovd_backend.Domain;
using __4.__2.moovd_backend.Services.SessionService;
using __4.__2.moovd_backend.Services.ConfigServices;
using __4.__2.moovd_backend.Services.DeviceServices.cs;
using __4.__2.moovd_backend.Services.SessionResultService;

namespace _4._2.moovd_backend.Installers
{
    public class DbInstaller : IInstaller
    {

        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
            options.UseSqlServer(
                configuration.GetConnectionString("DefaultConnection")
            ));
            services.AddDefaultIdentity<User>().AddRoles<IdentityRole>().AddEntityFrameworkStores<DataContext>();
            services.AddScoped<ISessionUserService, SessionUserService>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<IConfigService, ConfigService>();
            services.AddScoped<IDeviceService, DeviceService>();
            services.AddScoped<ISessionResultService, SessionResultService>();
        }

        public void InstallServices(IServiceCollection services, object configuration)
        {
            throw new System.NotImplementedException();
        }
    }
}