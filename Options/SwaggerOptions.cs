namespace _4._2.moovd_backend.Options{

public class SwaggerOptions{
    public string JsonRoute {get; set;}

    public string Description {get; set;}

    public string UiEndpoint {get; set;}
}}