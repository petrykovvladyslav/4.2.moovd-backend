using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _4._2.moovd_backend.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace _4._2.moovd_backend
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var serviceScope = host.Services.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                await dbContext.Database.MigrateAsync();

                var services = serviceScope.ServiceProvider;
                var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                if (!await roleManager.RoleExistsAsync("Therapist"))
                {
                    var therapistRole = new IdentityRole("Therapist");
                    await roleManager.CreateAsync(therapistRole);
                }

                if (!await roleManager.RoleExistsAsync("Patient"))
                {
                    var patientRole = new IdentityRole("Patient");
                    await roleManager.CreateAsync(patientRole);
                }

                var context = services.GetRequiredService<DataContext>();
                DbInitializer.Initialize(context);
            }

            await host.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
