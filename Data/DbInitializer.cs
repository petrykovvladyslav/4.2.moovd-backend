using System;
using System.Linq;
using __4.__2.moovd_backend.Domain;

namespace _4._2.moovd_backend.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            if (context.ControlConfigs.Any())
            {
                return;   // DB has been seeded
            }
            Guid guidId = Guid.Parse("6B29FC40-CA47-1067-B31D-00DD010662DA");
            float x = float.Parse("2.5");

            var controlConfigs = new ControlConfig[]
            {
            new ControlConfig{ControlConfigId = guidId ,CreationDate=DateTime.Parse("2005-09-01"),Velocity= x , GoalColor= "Blue", ColorInterval = 1, IsBall= false, IsHorizontal=false},
            };
            foreach (ControlConfig c in controlConfigs)
            {
                context.ControlConfigs.Add(c);
            }
            context.SaveChanges();
        }
    }
}