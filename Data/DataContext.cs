using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using _4._2.moovd_backend.Domain;
using __4.__2.moovd_backend.Domain;

namespace _4._2.moovd_backend.Data
{
    public class DataContext : IdentityDbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<SessionUser> SessionUser { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<ControlConfig> ControlConfigs { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<SessionResult> SessionResults { get; set; }
        public DbSet<User> User { get; set; }
    }
}