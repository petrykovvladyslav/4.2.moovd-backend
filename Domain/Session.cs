using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using __4.__2.moovd_backend.Domain;

namespace _4._2.moovd_backend.Domain
{
    public class Session
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime SessionDate { get; set; }
        public int ActiveSession { get; set; }

        public ICollection<Device> Devices { get; set; } = new Collection<Device>();
        public Guid ControlConfigId { get; set; }
        [ForeignKey("ControlConfigId")]
        public ControlConfig ControlConfig { get; set; } 

        public ICollection<SessionUser> SessionUsers { get; set; } = new Collection<SessionUser>();
        public ICollection<SessionResult> SessionResults { get; set; } = new Collection<SessionResult>();
    }
}