﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using __4.__2.moovd_backend.Domain;
using Microsoft.AspNetCore.Identity;

namespace _4._2.moovd_backend.Domain
{
    public class User : IdentityUser
    {
        public List<IdentityRole> Roles { get; set; }
        public int Active { get; set; }
        public string Gender { get; set; }
        public DateTime InsertTime { get; set; }
    }
}
