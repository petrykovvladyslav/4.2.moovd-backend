using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using _4._2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Domain
{
    public class Device
    {
        [Key]
        public Guid Id { get; set; }
        public string DeviceName { get; set; }



    }
}
