using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using _4._2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Domain
{
        public class ControlConfig
    {
        [Key]
        public Guid ControlConfigId { get; set; }
        public DateTime CreationDate { get; set; }
        public float Velocity { get; set; }
        public string GoalColor { get; set; }
        public int ColorInterval { get; set; }
        public bool IsBall { get; set; }
        public bool IsHorizontal { get; set; }
    }
}