using System;
using System.ComponentModel.DataAnnotations;

namespace __4.__2.moovd_backend.Domain
{
    public class Company
    {
        [Key]
        public Guid Id { get; set; }

        // public string[] DevicesIds { get; set; }
        public string Name { get; set; }

    }
}