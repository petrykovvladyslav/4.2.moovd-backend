using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _4._2.moovd_backend.Domain
{
    public class SessionResult
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public int TaskNumber { get; set; }
        public int ClickNumber { get; set; }
        public int ResponseTime { get; set; }
        public float Speed { get; set; }
        

    }
}
