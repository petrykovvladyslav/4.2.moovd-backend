using System.Collections.Generic;

namespace _4._2.moovd_backend.Domain
{

    public class AuthenticationResult
    {
        public string Token { get; set; }

        public bool Success { get; set; }

        public IEnumerable<string> Errors { get; set; }
    }
}