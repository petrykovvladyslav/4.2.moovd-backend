using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _4._2.moovd_backend.Domain
{
    public class SessionUser
    {
        [Key]
        public Guid Id { get; set; }

        public string UserType { get; set; }
        public Guid UserId { get; set; }

        public DateTime LoginDateTime { get; set; }
        public DateTime? LogoutDateTime { get; set; }
    }
}