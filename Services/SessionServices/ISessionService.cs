using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Services.SessionService
{
    public interface ISessionService
    {
        Task<List<Session>> GetSessionsAsync();
        Task<bool> CreateNewSession(Session session);
        Task<Session> GetSessionByIdAsync(Guid sessionId);
        Task<bool> UpdateSessionAsync(Session sessionToUpdate);
        Task<bool> DeleteSessionAsync(Guid sessionId);
    }
}