using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Data;
using _4._2.moovd_backend.Domain;
using Microsoft.EntityFrameworkCore;

namespace __4.__2.moovd_backend.Services.SessionService
{
    public class SessionService : ISessionService
    {

        private readonly DataContext _dataContext;

        public SessionService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> CreateNewSession(Session session)
        {
            await _dataContext.Sessions.AddAsync(session);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<bool> DeleteSessionAsync(System.Guid sessionId)
        {
            var session = await GetSessionByIdAsync(sessionId);

            if (session == null)
                return false;
            _dataContext.Sessions.Remove(session);
            var deleted = await _dataContext.SaveChangesAsync();
            return deleted > 0;

        }

        public async Task<List<Session>> GetSessionsAsync()
        {
            return await _dataContext.Sessions.Include(x => x.ControlConfig).Include(x => x.SessionUsers).Include(x => x.SessionResults).Include(x => x.Devices).ToListAsync();
        }

        public async Task<Session> GetSessionByIdAsync(System.Guid sessionId)
        {
            return await _dataContext.Sessions.Include(x => x.ControlConfig).Include(x => x.SessionUsers).Include(x => x.SessionResults).Include(x => x.Devices).SingleOrDefaultAsync(x => x.Id == sessionId);
        }

        public async Task<bool> UpdateSessionAsync(Session sessionToUpdate)
        {
            _dataContext.Sessions.Update(sessionToUpdate);
            var updated = await _dataContext.SaveChangesAsync();
            return updated > 0;

        }


    }
}
