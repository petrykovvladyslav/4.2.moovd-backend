using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Data;
using _4._2.moovd_backend.Domain;
using Microsoft.EntityFrameworkCore;

namespace __4.__2.moovd_backend.Services.SessionResultService
{
    public class SessionResultService : ISessionResultService
    {
        private readonly DataContext _dataContext;

        public SessionResultService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> CreateNewSessionResult(SessionResult sessionResult)
        {
            await _dataContext.SessionResults.AddAsync(sessionResult);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<SessionResult> GetSessionResultByIdAsync(Guid sessionResultId)
        {
            return await _dataContext.SessionResults.SingleOrDefaultAsync(x => x.Id == sessionResultId);
        }

        public async Task<List<SessionResult>> GetSessionResultsAsync()
        {
            return await _dataContext.SessionResults.ToListAsync();
        }
    }
}
