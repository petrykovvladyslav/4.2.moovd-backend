using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Services.SessionResultService
{
    public interface ISessionResultService
    {
        Task<List<SessionResult>> GetSessionResultsAsync();

        Task<bool> CreateNewSessionResult(SessionResult sessionResult);

        Task<SessionResult> GetSessionResultByIdAsync(Guid sessionResultId);
    }
}