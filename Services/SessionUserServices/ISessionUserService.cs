using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Services.SessionUserServices
{
    public interface ISessionUserService
    {
        Task <bool> CreateNewSessionUser(SessionUser sessionUser);
         Task<List<SessionUser>> GetSessionUsersAsync();
        Task<SessionUser> GetSessionUserByIdAsync(Guid sessionUserId);
    }
}