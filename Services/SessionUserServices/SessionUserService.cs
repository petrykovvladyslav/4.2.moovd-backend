using System.Collections.Generic;
using System.Threading.Tasks;
using _4._2.moovd_backend.Data;
using _4._2.moovd_backend.Domain;
using Microsoft.EntityFrameworkCore;

namespace __4.__2.moovd_backend.Services.SessionUserServices
{
    public class SessionUserService : ISessionUserService
    {

        private readonly DataContext _dataContext;
        
        public SessionUserService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

       public async Task<bool> CreateNewSessionUser(SessionUser sessionUser)
        {
            await _dataContext.SessionUser.AddAsync(sessionUser);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<SessionUser> GetSessionUserByIdAsync(System.Guid sessionUserId)
        {
            return await _dataContext.SessionUser.SingleOrDefaultAsync(x => x.Id == sessionUserId);
        }

        public async Task<List<SessionUser>> GetSessionUsersAsync()
        {
            return await _dataContext.SessionUser.ToListAsync();
        }
    }
}