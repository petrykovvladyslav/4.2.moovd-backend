using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Domain;
using _4._2.moovd_backend.Data;
using Microsoft.EntityFrameworkCore;

namespace __4.__2.moovd_backend.Services.ConfigServices
{
    public class ConfigService : IConfigService
    {

        private readonly DataContext _dataContext;

        public ConfigService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<bool> CreateNewConfig(ControlConfig config)
        {
            await _dataContext.ControlConfigs.AddAsync(config);
            var created = await _dataContext.SaveChangesAsync();
            _dataContext.SaveChanges();
            return created > 0;
        }

        public async Task<ControlConfig> GetConfigByIdAsync(Guid configId)
        {
             return await _dataContext.ControlConfigs.SingleOrDefaultAsync(x => x.ControlConfigId == configId);
        }

        public async Task<List<ControlConfig>> GetConfigsAsync()
        {
            return await _dataContext.ControlConfigs.ToListAsync();
        }
    }
}