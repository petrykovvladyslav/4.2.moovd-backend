using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Services.ConfigServices
{
    public interface IConfigService
    {
        Task<List<ControlConfig>> GetConfigsAsync();
        Task<bool> CreateNewConfig(ControlConfig config);
        Task<ControlConfig> GetConfigByIdAsync(Guid configId);


    }
}