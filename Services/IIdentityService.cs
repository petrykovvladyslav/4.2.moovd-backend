using System.Threading.Tasks;
using _4._2.moovd_backend.Domain;

namespace _4._2.moovd_backend.Services
{
    public interface IIdentityService
    {
        Task<AuthenticationResult> RegisterAsync(string email, string password, string type);
        Task<AuthenticationResult> LoginAsync(string email, string password);

    }
}