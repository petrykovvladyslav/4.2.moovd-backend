using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Domain;

namespace __4.__2.moovd_backend.Services.DeviceServices.cs
{
    public interface IDeviceService
    {
        Task<List<Device>> GetDevicesAsync();
        Task<bool> CreateNewDevice(Device device);
        Task<Device> GetDeviceByIdAsync(Guid deviceId);
    }
}