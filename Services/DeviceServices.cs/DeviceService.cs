using System.Collections.Generic;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Domain;
using _4._2.moovd_backend.Data;
using Microsoft.EntityFrameworkCore;

namespace __4.__2.moovd_backend.Services.DeviceServices.cs
{
    public class DeviceService : IDeviceService
    {
        private readonly DataContext _dataContext;

        public DeviceService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<bool> CreateNewDevice(Device device)
        {
            await _dataContext.Devices.AddAsync(device);
            var created = await _dataContext.SaveChangesAsync();
            return created > 0;
        }

        public async Task<Device> GetDeviceByIdAsync(System.Guid deviceId)
        {
            return await _dataContext.Devices.SingleOrDefaultAsync(x => x.Id == deviceId);
        }

        public async Task<List<Device>> GetDevicesAsync()
        {
            return await _dataContext.Devices.ToListAsync();
        }
    }
}