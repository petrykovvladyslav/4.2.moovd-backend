using System;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Contracts.V1.Requests.ControlConfigs;
using __4.__2.moovd_backend.Domain;
using __4.__2.moovd_backend.Services.ConfigServices;
using __4.__2.moovd_backend.Services.SessionService;
using _4._2.moovd_backend.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace __4.__2.moovd_backend.Controllers.V1
{
    public class ConfigController : ControllerBase
    {
        private readonly IConfigService _configService;
        private readonly ISessionService _sessionService;

        public ConfigController(IConfigService configService, ISessionService sessionService)
        {
            _configService = configService;
            _sessionService = sessionService;
        }

        [HttpGet(ApiRoutes.ConfigRoute.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _configService.GetConfigsAsync());
        }

        [HttpPost(ApiRoutes.ConfigRoute.Create)]
        public async Task<IActionResult> Create([FromBody] CreateConfig configPost)
        {
            // var session = await _sessionService.GetSessionByIdAsync(Guid.Parse(configPost.SessionId));
            var config = new ControlConfig
            {
                CreationDate = configPost.CreationDate,
                Velocity = configPost.Velocity,
                GoalColor = configPost.GoalColor,
                ColorInterval = configPost.ColorInterval,
                IsBall = configPost.IsBall,
                IsHorizontal = configPost.IsHorizontal
            };

            await _configService.CreateNewConfig(config);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.ConfigRoute.Get.Replace("{configId}", config.ControlConfigId.ToString());

            return Created(locationUri, config);
        }
    }
}