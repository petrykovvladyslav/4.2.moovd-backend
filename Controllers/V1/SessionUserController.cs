using System;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Contracts.V1.Requests;
using __4.__2.moovd_backend.Services.SessionUserServices;
using _4._2.moovd_backend.Contracts;
using _4._2.moovd_backend.Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace __4.__2.moovd_backend.Controllers.V1
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SessionUserController : ControllerBase
    {
        private readonly ISessionUserService _userSessionService;
        public SessionUserController(ISessionUserService userSessionService)
        {
            _userSessionService = userSessionService;
        }

        [HttpPost(ApiRoutes.SessionUserRoute.Create)]
        public async Task<IActionResult> Create([FromBody] CreateSessionUser sessionUserPost)
        {
            var sessionUser = new SessionUser
            {
                UserType = sessionUserPost.UserType,
                UserId = sessionUserPost.UserId,
                LoginDateTime = sessionUserPost.LoginDateTime,
                LogoutDateTime = (DateTime?)null 
            };

            await _userSessionService.CreateNewSessionUser(sessionUser);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.SessionUserRoute.Get.Replace("{sessionUserId}", sessionUser.Id.ToString());

            return Created(locationUri, sessionUser);
        }

    }
}