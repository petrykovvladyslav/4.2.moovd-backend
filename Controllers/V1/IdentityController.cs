using System.Linq;
using System.Threading.Tasks;
using _4._2.moovd_backend.Contracts;
using _4._2.moovd_backend.Services;
using _4._2.moovd_backend.V1.Responses;
using Microsoft.AspNetCore.Mvc;

namespace _4._2.moovd_backend.Controllers
{
    public class IdentitiyController : ControllerBase
    {
        public readonly IIdentityService _identityService;

        public IdentitiyController(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        [HttpPost(ApiRoutes.Identity.Register)]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequest request)
        {

            if(!ModelState.IsValid){
                return BadRequest(new AuthFailedResponse{
                    Errors = ModelState.Values.SelectMany(x => x.Errors.Select(xx => xx.ErrorMessage))
                });
            }

            var authResponse = await _identityService.RegisterAsync(request.Email, request.Password, request.Type);
            
            if (!authResponse.Success){
                return BadRequest(new AuthFailedResponse{
                    Errors = authResponse.Errors
                });
            }
            return Ok(new AuthSuccessResponse{
                Token = authResponse.Token
            });
        }

        [HttpPost(ApiRoutes.Identity.Login)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequest request)
        {
            var authResponse = await _identityService.LoginAsync(request.Email, request.Password);
            
            if (!authResponse.Success){
                return BadRequest(new AuthFailedResponse{
                    Errors = authResponse.Errors
                });
            }
            
            return Ok(new AuthSuccessResponse{
                Token = authResponse.Token
            });
        }

    }
}