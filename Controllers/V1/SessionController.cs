using System;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Contracts.V1.Requests.Session;
using __4.__2.moovd_backend.Services.ConfigServices;
using __4.__2.moovd_backend.Services.DeviceServices.cs;
using __4.__2.moovd_backend.Services.SessionResultService;
using __4.__2.moovd_backend.Services.SessionService;
using __4.__2.moovd_backend.Services.SessionUserServices;
using _4._2.moovd_backend.Contracts;
using _4._2.moovd_backend.Domain;
using Microsoft.AspNetCore.Mvc;

namespace __4.__2.moovd_backend.Controllers.V1
{
    public class SessionController : ControllerBase
    {
        private readonly ISessionService _sessionService;
        private readonly ISessionUserService _sessionUserService;
        private readonly ISessionResultService _sessionResultService;
        private readonly IDeviceService _deviceService;
        private readonly IConfigService _configService;
        public SessionController(ISessionService sessionService, ISessionUserService sessionUserService, ISessionResultService sessionResultService, IDeviceService deviceService, IConfigService configService)
        {
            _sessionService = sessionService;
            _sessionUserService = sessionUserService;
            _sessionResultService = sessionResultService;
            _deviceService = deviceService;
            _configService = configService;
        }

        [HttpGet(ApiRoutes.SessionRoute.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _sessionService.GetSessionsAsync());
        }

        [HttpGet(ApiRoutes.SessionRoute.Get)]
        public async Task<IActionResult> Get([FromRoute] Guid sessionId)
        {

            var getSession = await _sessionService.GetSessionByIdAsync(sessionId);

            if (getSession == null)
                return NotFound();

            return Ok(getSession);
        }


        [HttpPut(ApiRoutes.SessionRoute.Update)]
        public async Task<IActionResult> Update([FromRoute] Guid sessionId, [FromBody] UpdateSession request)
        {
            var session = await _sessionService.GetSessionByIdAsync(sessionId);
            
            //Check length of session.SessionUsers. if length > 1 change session.ActiveSession
            if (request.SessionUsers != null)
            {
                var sessionUser = await _sessionUserService.GetSessionUserByIdAsync(Guid.Parse(request.SessionUsers));
                session.SessionUsers.Add(sessionUser);
            }
            if (request.SessionResultId != null)
            {
                var sessionResult = await _sessionResultService.GetSessionResultByIdAsync(Guid.Parse(request.SessionResultId));
                session.SessionResults.Add(sessionResult);
            }

            if (request.ActiveSession != 0){
                session.ActiveSession = request.ActiveSession;
            }

            var updated = await _sessionService.UpdateSessionAsync(session);

            if (updated)
                return Ok(session);

            return NotFound();

        }

        [HttpPost(ApiRoutes.SessionRoute.Create)]
        public async Task<IActionResult> Create([FromBody] CreateSession sessionPost)
        {
            var configObject = await _configService.GetConfigByIdAsync(Guid.Parse(sessionPost.ConfigId));
            var session = new Session
            {
                ControlConfigId = Guid.Parse(sessionPost.ConfigId),
                SessionDate = sessionPost.SessionDate,
                ActiveSession = sessionPost.ActiveSession
            };
            if (sessionPost.SessionUsers.Length > 10)
            {
                var sessionUser = await _sessionUserService.GetSessionUserByIdAsync(Guid.Parse(sessionPost.SessionUsers));
                session.SessionUsers.Add(sessionUser);
            }
            await _sessionService.CreateNewSession(session);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.SessionRoute.Get.Replace("{sessionId}", session.Id.ToString());

            return Created(locationUri, session);
        }
        [HttpDelete(ApiRoutes.SessionRoute.Delete)]
        public async Task<IActionResult> Delete([FromRoute] Guid sessionId)
        {
            var deleted = await _sessionService.DeleteSessionAsync(sessionId);

            if (deleted)
                return NoContent();

            return NotFound();
        }
    }
}