using System;
using System.Threading.Tasks;
using __4.__2.moovd_backend.Contracts.V1.Requests.SessionResult;
using __4.__2.moovd_backend.Services.SessionResultService;
using _4._2.moovd_backend.Contracts;
using _4._2.moovd_backend.Domain;
using Microsoft.AspNetCore.Mvc;

namespace __4.__2.moovd_backend.Controllers.V1
{
    // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SessionResultController : ControllerBase
    {
        private readonly ISessionResultService _sessionResultService;
        public SessionResultController(ISessionResultService sessionResultService)
        {
            _sessionResultService = sessionResultService;
        }

        [HttpGet(ApiRoutes.SessionResultRoute.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _sessionResultService.GetSessionResultsAsync());
        }

        [HttpGet(ApiRoutes.SessionResultRoute.Get)]
        public async Task<IActionResult> Get([FromRoute] Guid sessionResultId)
        {

            var getSessionResult = await _sessionResultService.GetSessionResultByIdAsync(sessionResultId);

            if (getSessionResult == null)
                return NotFound();

            return Ok(getSessionResult);
        }


        [HttpPost(ApiRoutes.SessionResultRoute.Create)]
        public async Task<IActionResult> Create([FromBody] CreateSessionResult sessionResultPost)
        {
            var sessionResult = new SessionResult
            {
                TimeStamp = sessionResultPost.TimeStamp,
                TaskNumber = sessionResultPost.TaskNumber,
                ClickNumber = sessionResultPost.ClickNumber,
                ResponseTime = sessionResultPost.ResponseTime,
                Speed = sessionResultPost.Speed,
                // SessionId = sessionResultPost.SessionId
            };

            await _sessionResultService.CreateNewSessionResult(sessionResult);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.SessionResultRoute.Get.Replace("{sessionResultId}", sessionResult.Id.ToString());

            return Created(locationUri, sessionResult);
        }



    }
}