using System.Threading.Tasks;
using __4.__2.moovd_backend.Contracts.V1.Requests.Device;
using __4.__2.moovd_backend.Domain;
using __4.__2.moovd_backend.Services.ConfigServices;
using __4.__2.moovd_backend.Services.DeviceServices.cs;
using _4._2.moovd_backend.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace __4.__2.moovd_backend.Controllers.V1
{
    public class DeviceController : ControllerBase
    {
        private readonly IDeviceService _deviceService;
        
        public DeviceController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        [HttpGet(ApiRoutes.DeviceRoute.GetAll)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _deviceService.GetDevicesAsync());
        }

        [HttpPost(ApiRoutes.DeviceRoute.Create)]
        public async Task<IActionResult> Create([FromBody] CreateDevice devicePost)
        {
            var device = new Device
            {
                DeviceName = devicePost.DeviceName
            };

            await _deviceService.CreateNewDevice(device);

            var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
            var locationUri = baseUrl + "/" + ApiRoutes.DeviceRoute.Get.Replace("{deviceId}", device.Id.ToString());

            return Created(locationUri, device);
        }
    }
}